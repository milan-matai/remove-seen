#### Installation
- Follow [these steps](https://developer.chrome.com/extensions/getstarted) to add the extension

#### Cancel script to show "seens" again for current tab
- Open console on Facebook and follow the details (script logging with "### Remove Seen" prefix)
