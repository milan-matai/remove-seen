let log = (...params) => {
  const LOG_PREFIX = '### Remove Seen';
  let args = [LOG_PREFIX + " - ", ...params];
  console.log.apply(console, args);
};

let removeSeenClasses = () => {
  let seenClasses = '.seen, .seenByAll';
  document.querySelectorAll(seenClasses).forEach(el => {
    el.remove();
  });
};

let init = () => {
  const INTERVAL_MS = 200;
  let seen = setInterval(removeSeenClasses, INTERVAL_MS);
  log(`Script started, removing seen in every ${INTERVAL_MS} ms`);
  log(`You can cancel removing seens by calling clearInterval(${seen})`);
};

init();
